import java.time.Duration;
import java.time.Instant;
import java.time.LocalTime;
import java.util.*;

public class Answer {

    public static void main(String[] param) {

        // TODO!!! Solutions to small problems
        //   that do not need an independent method!

        // conversion double -> String
        double dbl = 2.2;
        System.out.println(Double.toString(dbl));

        // conversion String -> int
        String str = "22";
        try {
            System.out.println(Integer.parseInt(str));
        } catch (NumberFormatException err) {
            System.out.println(err.toString());
        }

        // "hh:mm:ss"
        LocalTime time = LocalTime.now();
        System.out.println(time.toString().substring(0, 8));

        // cos 45 deg
        System.out.println(Math.cos(Math.toRadians(45)));

        // table of square roots
        for (int i = 0; i < 100; i = i + 5) {
            System.out.println(Math.sqrt(i));
        }

        String firstString = "ABcd12";
        String result = reverseCase(firstString);
        System.out.println("\"" + firstString + "\" -> \"" + result + "\"");

        // reverse string
        String randomString = "RandomString";
        System.out.println(new StringBuilder(randomString).reverse().toString());

        String s = "How  many	 words   here";
        int nw = countWords(s);
        System.out.println(s + "\t" + String.valueOf(nw));

        // pause. COMMENT IT OUT BEFORE JUNIT-TESTING!
        // https://stackoverflow.com/questions/4927856/how-to-calculate-time-difference-in-java - some reference from here
//        Instant start = Instant.now();
//        try {
//            Thread.sleep(3000);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
//        Instant end = Instant.now();
//        Duration timeElapsed = Duration.between(start, end);
//        System.out.println("Time elapsed: " + timeElapsed.toMillis());

        final int LSIZE = 100;
        ArrayList<Integer> randList = new ArrayList<Integer>(LSIZE);
        Random generaator = new Random();
        for (int x = 0; x < LSIZE; x++) {
            randList.add(generaator.nextInt(1000));
        }

        // minimal element
        System.out.println(Collections.min(randList));

        // HashMap tasks:
        //    create
        HashMap<String, String> schoolMap = new HashMap<>();
        schoolMap.put("ICD0001", "Algoritmid ja andmestruktuurid");
        schoolMap.put("ICA0013", "Fundamentals of networking");
        schoolMap.put("ICD0009", "Building Distributed Systems");
        schoolMap.put("ICA0005", "Andmebaasisüsteemide alused");
        schoolMap.put("ICY0029", "Ettevõtluse alused");

        //    print all keys
        for (String subject : schoolMap.keySet()) {
            System.out.println(subject);
        }
        System.out.println("------------------------");
        //    remove a key
        schoolMap.remove("ICA0005");
        //    print all pairs
        schoolMap.forEach((key, value) -> System.out.println(key + " " + value));

        System.out.println("Before reverse:  " + randList);
        reverseList(randList);
        System.out.println("After reverse: " + randList);

        System.out.println("Maximum: " + maximum(randList));
    }

    /**
     * Finding the maximal element.
     *
     * @param a Collection of Comparable elements
     * @return maximal element.
     * @throws NoSuchElementException if <code> a </code> is empty.
     */
    static public <T extends Object & Comparable<? super T>>
    T maximum(Collection<? extends T> a)
            throws NoSuchElementException {
        return Collections.max(a);
    }

    /**
     * Counting the number of words. Any number of any kind of
     * whitespace symbols between words is allowed.
     *
     * @param text text
     * @return number of words in the text
     */
    public static int countWords(String text) {
        if (text.length() == 0) return 0;
        String txt = text.replaceAll("\\t+", " ");
        String[] list = txt.split(" ");
        int count = 0;
        for (String item : list) {
            if (!item.equals("")) {
                count++;
            }
        }
        return count;
    }

    /**
     * Case-reverse. Upper -> lower AND lower -> upper.
     *
     * @param s string
     * @return processed string
     */
    public static String reverseCase(String s) {

        StringBuffer res = new StringBuffer();

        for (int i = 0; i < s.length(); i++) {
            if (!Character.isAlphabetic(s.charAt(i))) {
                res.append(s.charAt(i));
            }
            if (Character.isUpperCase(s.charAt(i))) {
                res.append(Character.toLowerCase(s.charAt(i)));
            }
            if (Character.isLowerCase(s.charAt(i))) {
                res.append(Character.toUpperCase(s.charAt(i)));
            }
        }

        return res.toString();
    }

    /**
     * List reverse. Do not create a new list.
     *
     * @param list list to reverse
     */
    public static <T extends Object> void reverseList(List<T> list)
            throws UnsupportedOperationException {
        Collections.reverse(list);
    }
}